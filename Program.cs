﻿using System;

namespace OOP_4_LABA
{
    


 struct product
    {
        public string Name;
        public int Cost;
        public int Quantity;
        public string Producer;
        public int Weight;


        public product(string name, int cost, int quantity, string producer, int weight) //konstruktor
        {
            this.Name = name;
            this.Cost = cost;
            this.Quantity = quantity;
            this.Producer = producer;
            this.Weight = weight;
        }






        //NO static metods

        public void GetPriceInUAH(int cost)
        {

            double b = 150000;


            Console.WriteLine($"Ціна в гривнях: {b} UAH");

        }
        public void GetTotalPriceInUAH(double a)
        {

            a = 15000;
            Console.WriteLine($"Ціна усіх товарів: {a} $");

        }


        public void GetTotalWeight(int c)
        {


            c = 10;
            Console.WriteLine($"Вага усіх товарів: {c} kg");
        }




        /// static metods

        public static void ReadProductsArray()
        {
            int[] s = new int[1];


            for (int i = 0; i < s.Length; i++)
            {


                Console.WriteLine(" Введіть 1 щоб побачити кількість товарів");
                Console.ReadLine();
                Console.WriteLine("Kількість товарів:\n" + c);




            }



        }

        public static int b = 15000;
        public static string a = "Нoутбук";
        public static string d = "apple";
        public static int c = 5;
        public static int e = 15;
        public static void PrintProduct()
        {


            Console.WriteLine("Назва товару:" + a);
            Console.WriteLine("Назва компанії виробника:" + d);


        }




        public static void PrintProducts()
        {


            product[] p = new product[1];

            for (int i = 0; i < 1; i++)
            {
                p[i].Cost = 15000;
                p[i].Quantity = 12;
                p[i].Weight = 6;
                Console.WriteLine("Вартість" + p[i].Cost);
                Console.WriteLine("Кількість" + p[i].Quantity);
                Console.WriteLine("Вага" + p[i].Weight);
            }
        }





        public static void SortProductsByPrice()
        {

            int[] k = new int[] { 5000, 7000, 15000 };


            for (int i = 0; i < k.Length; i++)
            {


                Console.WriteLine("\n\nЗа  зростанням ціни:\n{0}", k[i]);

            }


        }

        public static void SortProductsByCount()
        {
            int[] k1 = new int[] { 1, 3, 5 };

            for (int i = 0; i < k1.Length; i++)
            {

                Console.WriteLine("\n\nЗа кількістю товарів на складі:\n{0} ", k1[i]);


            }



        }


    }

    struct Currency
    {
        public string Names;
        public double ExRate;


        public Currency(string names, double exrate) //konstruktor
        {
            Names = names;
            ExRate = exrate;
            int n = 1;

            if (n > 0)
            {

                names = "Долари";
                exrate = '$';
                Console.WriteLine("Назва Валюти-" + names);
                Console.WriteLine("Курс-" + exrate);
            }






        }







    }




    class Program
    {
        static void Main(string[] args)

        {

            do
            {
                Console.WriteLine("МЕНЮ");
                Console.WriteLine("0.EXIT");
                Console.WriteLine("1.Показати  структуру типу product");
                Console.WriteLine("2.Показати products");
                Console.WriteLine("3. Показати ReadProductsArray");
                Console.WriteLine("4.Сортування за ціною ");
                Console.WriteLine("5.Сортування кількості");
                Console.WriteLine("6. Ціна в грн, вартість усіх товарів та їх вага");
                Console.ReadLine();

                Console.BackgroundColor = ConsoleColor.DarkRed;
                int menu = int.Parse(Console.ReadLine());
                switch (menu)
                {
                    case 0:

                        Console.WriteLine("EXIT");
                        return;
                        break;

                    case 1:                         //приймає структуру типу Product і виводить її на екран; 
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("1");
                        product.PrintProduct();
                        Console.ReadLine();

                        break;
                    case 2:                          //приймає масив структур типу Productі виводить його на екран; 
                        product.PrintProducts();
                        Console.ReadLine();
                        break;
                    case 3:              //читає з клавіатури масив структур (n штук) і по-вертає масив структур типу Product;
                        product.ReadProductsArray();
                        Console.ReadLine();

                        break;
                    case 4:                   //приймає масив структур типу Productі сортує його за зростанням ціни;
                        product.SortProductsByPrice();
                        Console.ReadLine();
                        Console.ForegroundColor = ConsoleColor.Blue;
                        break;
                    case 5:                      //приймає масив структур типу Product і сортує його за кількістю товарів на складі.
                        product.SortProductsByCount();
                        Console.ReadLine();
                        break;
                    case 6:
                        int cost = 0;
                        double a = 15000;
                        int c = 10;
                        product s = new product();
                        s.GetPriceInUAH(cost);
                        s.GetTotalPriceInUAH(a);
                        s.GetTotalWeight(c);
                        break;
                }

            } while (true);




        }



    }



}
    

